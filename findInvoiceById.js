
const client = require('./client')

//default uuids: '00000000-0000-0000-0000-000000000001' bis '00000000-0000-0000-0000-000000000004'
let Uuid = {
    UUID: '00000000-0000-0000-0000-000000000001'
}

client.FindInvoiceById(Uuid, (error, message) => {

    if (!error) {
        if (message.Product == null) {

            console.log('Keine Invoice gefunden')
        } else {

            console.log('Hier ist die gefundene Invoice: ', message)
        }
    } else {

        console.error(error)
    }
})