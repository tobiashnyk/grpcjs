# Anleitung GRPc mit Javascript

GRPc mit UUIDs von node-uuid für InvoiceIDs

node-uuid ist eins der verbreitetsten packages zum generieren von UUIDs.


## 1. Projekt mit Git Klonen

einen neuen Ordner für das Projekt anlegen und mit `git clone https://tobiashnyk@bitbucket.org/tobiashnyk/grpcjs.git` Klonen


## 2. Server starten

Eine Powershell in dem geklonten Projektordner öffnen uns `node --no-deprecation index.js` ausführen.

## 3. Client verwenden

Eine zweite (client)Powershell in dem geklonten Projektordner öffnen um Skripte auszuführen.

### Um alle Nachrichten (Invoices) auszugeben:

 ``` node --no-deprecation .\GetAllMessages.js ```

### Um eine neue Invoice zu erstellen:

 ``` node --no-deprecation .\createInvoice.js ```

 * default:
		{	ClientId: '2',
			Number: 5
			UUID: [autogenerated]
			product: {
				ProductId: '1',
				Preis: 109.99,
				Hersteller: 'Bosch',
				Beschreibung: 'Neuer Herd'
			}
		}

Details um die Invoice zu ändern stehen in `CreateInvoice.js`

### Um die neue Invoice zu finden:

Die UUID von der Konsole kopieren und in `FindInvoiceById.js` die default UUID:
``` 
	let Uuid = {
    UUID: '00000000-0000-0000-0000-000000000001'
	} 
```
mit dem Konsolenstring ersetzen. Danach die Änderrung speichern und in der client Powershell
`node --no-deprecation FindInvoiceById.js` ausführen.

### Um die Summe der Verkäufe eines Produktes von allen Invoices zu erhalten:

``` node --no-deprecation .\GetColumeOfSales.js ```

* default: Summe der Produkte mit ProduktId = 3

Details zum Ändern des Produkts stehen in `GetVolumeOfSales.js`

## Server Stoppen

in der client Powershell `node --no-deprecation .\Shutdown.js` ausführen
