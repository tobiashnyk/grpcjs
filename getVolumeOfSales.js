
const client = require('./client')

//ProductId: �ndern um die Summe anderer Produkte anzeigen zu lassen
let Product = {

    //default ProductIds: '1' bis '3'
    ProductId: '3'
}

client.GetVolumeOfSales(Product, (error, message) => {

    if (!error) { 

        console.log(Math.round(message.Volume * 100) / 100)
    } else {

        throw new Exception(error)
    }
})

function Exception(error) {

    this.details = error.details;
    this.code = error.code;
    this.metadata = error.metadata;
}