
const grpc = require('grpc')
const server = new grpc.Server()
const uuid = require('uuid/v1')

//eigentlich von DB
var ProductIds = ['1', '2', '3']
var ClientIds = ['1', '2', '3']

const messagesProto = grpc.load('messages.proto')

const messages = [
    //Liste von Invoices

    //Default Werte um ein paar Daten zu haben
    //eigentlich auch aus DB �ber UUID
    {
        ClientId: '1',
        Product: {
            ProductId: '1',
            Preis: 109.99,
            Hersteller: 'Bosch',
            Beschreibung: 'Herd'
        },
        Number: 10,
        UUID: '00000000-0000-0000-0000-000000000001'
    },
    {
        ClientId: '2',
        Product: {
            ProductId: '2',
            Preis: 6.87,
            Hersteller: 'Edeka',
            Beschreibung: 'Kekse'
        },
        Number: 20,
        UUID: '00000000-0000-0000-0000-000000000002'
    },
    {
        ClientId: '3',
        Product: {
            ProductId: '3',
            Preis: 10.99,
            Hersteller: 'McDonals',
            Beschreibung: 'Burger'
        },
        Number: 30,
        UUID: '00000000-0000-0000-0000-000000000003'
    },
    {
        ClientId: '1',
        Product: {
            ProductId: '3',
            Preis: 10.99,
            Hersteller: 'McDonals',
            Beschreibung: 'Burger'
        },
        Number: 40,
        UUID: '00000000-0000-0000-0000-000000000004'
    }
]

server.addService(messagesProto.MessageService.service, {

    GetMessages: (_, callback) => {

        //gibt MessageList aus
        callback(null, messages)
    },

    CreateInvoice: (call, callback) => {

        let message = call.request

        if (!ClientIds.includes(message.ClientId)) {

            return callback({

                //returns Unknown Client Exception
                code: grpc.status.NOT_FOUND,
                message: 'Client nicht gefunden.'
            })
        } else if (!ProductIds.includes(message.Product.ProductId)) {

            return callback({

                //returns Unknown Product Exception
                code: grpc.status.NOT_FOUND,
                message: 'Produkt nicht gefunden.'
            })
        } else {

            //set UUID f�r Invoice
            message.UUID = uuid()

            //f�gt die Invoice zu MessageList hinzu
            messages.push(message)

            //Baut String f�r die R�ckgabe
            let Nachricht = "ClientId: " + message.ClientId + "\r\n"
            Nachricht += "Number: " + message.Number + "\r\n"
            Nachricht += "UUID: " + message.UUID + "\r\n"
            Nachricht += "Produkt: " + "\r\n"
            Nachricht += "\t ProduktId: " + message.Product.ProductId + "\r\n"
            Nachricht += "\t Preis: " + message.Product.Preis + "\r\n"
            Nachricht += "\t Hersteller: " + message.Product.Hersteller + "\r\n"
            Nachricht += "\t Beschreibung: " + message.Product.Beschreibung + "\r\n"
            
            callback(null, Nachricht)
        }
    },

    FindInvoiceById: (call, callback) => {

        let message = call.request
        let Nachricht = ''

        //sucht in MessageList nach der Invoice mit der gesuchten Id x = L�ufer
        for (let x = 0; x < messages.length; x++) {
            
            if (messages[x].UUID == message.UUID) {
                console.log('Es wurde eine Nachricht gesucht:')
                //gibt die message auf der Konsole aus
                //console.log(messages[x])
                Nachricht = messages[x]
                break;
            }
        }

        callback(null, Nachricht)
    },

    GetVolumeOfSales: (call, callback) => {

        let message = call.request
        let Volume = 0

        //sucht ob das Produkt in unserer Produktdatenbank ist
        if (!ProductIds.includes(message.ProductId)) {

            return callback({

                //returns Unknown Product Exception
                code: grpc.status.NOT_FOUND,
                message: 'Produkt nicht gefunden.'
            })
        }

        //sucht alle Invoices, bei denen das gesuchte Produkt vorkommt und summiert die Anzahl der gekauften Produkte * deren Preis x ist der L�ufer
        for (x = 0; x < messages.length; x++) {

            if (messages[x].Product.ProductId == message.ProductId) {

                Volume += messages[x].Number * messages[x].Product.Preis
            }
        }

        callback(null, parseFloat(Volume))
    },

    Shutdown: (_, callback) => {

        console.log('Server wird heruntergefahren')
        server.forceShutdown()
        
    }
})

server.bind('127.0.0.1:50051', grpc.ServerCredentials.createInsecure())
console.log('Server gestartet: http://127.0.0.1:50051')
server.start()