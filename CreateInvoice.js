
const client = require('./client')

// DIESE WERTE �NDERN
let newMessage = {

    //default ClientIds: '1' bis '3' 
    //BEI ANDEREN WERTEN: den neuen Wert bei ClientIds in Index.js hinzuf�gen, sonst wird eine Exception geworfen
    ClientId: '2',
    /*eigentlich von aufrufender Instanz �bergeben*/

    //hier m�ssen produkte angegeben werden (die default Vorschl�ge unten k�nnen so �bernommen werden)
    //BEI ANDEREN PRODUCTIDS ALS UNTEN: ProduktId muss bei ProductIds in Index.js hinzuf�gen, sonst wird eine Exception geworfen
    Product: {
    /*eigentlich von Webserver oder aufrufender Instanz �bergeben*/

        //default ProductIds: '1' bis '3'
        ProductId: '1',
        Preis: 109.99,
        Hersteller: 'Bosch',
        Beschreibung: 'Neuer Herd'
    },
    Number: 5
}

client.CreateInvoice(newMessage, (error, message) => {
    
    if (!error) {

        console.log('Invoice erfolgreich erst')
        console.log(message.Invoice)
    } else {

        throw new Exception(error);
    }
})

function Exception(error) {

    this.details = error.details;
    this.code = error.code;
    this.metadata = error.metadata;
}


/*
Produkt 1:
product: {

        ProductId: '1',
        Preis: 109.99,
        Hersteller: 'Bosch',
        Beschreibung: 'Herd'
    }

Produkt 2:
product: {
            ProductId: '2',
            Preis: 6,
            Hersteller: 'Edeka',
            Beschreibung: 'Kekse'
        }

Produkt 3:
product: {
            ProductId: '3',
            Preis: 1,
            Hersteller: 'McDonals',
            Beschreibung: 'Burger'
        }
*/