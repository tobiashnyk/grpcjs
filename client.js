//Invoice findInvoiceById(String number)
//Float getVolumeOfSales(Product p) throws UnknownProductException
//String createInvoice(String clientId, Product p, Integer number) throws UnknownProduct, UnknownClientException

const grpc = require('grpc')
const PROTO_PATH = './messages.proto'
const MessageService = grpc.load(PROTO_PATH).MessageService
const client = new MessageService('localhost:50051', grpc.credentials.createInsecure())

module.exports = client